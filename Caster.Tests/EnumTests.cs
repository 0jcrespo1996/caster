﻿using Xunit;

namespace Caster.Tests;

public class EnumTests
{
    private enum Direction
    {
        Forward = 0,
        Backwards = 1,
        Left = 2,
        Right = 3
    }

    private enum Angle
    {
        Zero = 0,
        OneEighty = 1,
        TwoSeventy = 2,
        Ninety = 3
    }

    [Fact]
    public void DirectionToAngle()
    {
        const Direction left = Direction.Left;
        var angle = Cast<Direction, Angle>.Checked(left);
        Assert.Equal(Angle.TwoSeventy, angle);
    }
    
    [Fact]
    public void AngleToDirection()
    {
        const Angle ninety = Angle.Ninety;
        var direction = Cast<Angle, Direction>.Checked(ninety);
        Assert.Equal(Direction.Right, direction);
    }
    
    [Fact]
    public void IntToDirection()
    {
        var direction = Cast<int, Direction>.Checked(0);
        Assert.Equal(Direction.Forward, direction);
    }

    [Fact]
    public void DirectionToInt()
    {
        const Direction direction = Direction.Right;
        var intValue = Cast<Direction, int>.Checked(direction);
        Assert.Equal(3, intValue);
    }
    
}