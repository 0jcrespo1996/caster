﻿using System;
using Xunit;

namespace Caster.Tests;

public class ObjectTests
{
    [Fact]
    public void StringObjectToString()
    {
        object obj = "test";
        var res = Cast<object, string>.Unchecked(obj);
        Assert.Equal("test", res);
    }

    [Fact]
    public void NullToString()
    {
        string? str = null;
        var res = Cast<string?, string>.Unchecked(str);
        Assert.Null(res);
    }

    [Fact]
    public void ObjectToString()
    {
        var obj = new object();
        Assert.Throws<InvalidCastException>(() => Cast<object, string>.Unchecked(obj));
    }
}