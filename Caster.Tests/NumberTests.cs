﻿using System;
using Xunit;

namespace Caster.Tests;

public class NumberTests
{
    [Fact]
    public void Int64ToInt32()
    {
        const long value = int.MaxValue;
        var result = Cast<long, int>.Checked(value);
        Assert.Equal(value, result);
    }
    
    [Fact]
    public void Int64ToInt32_Overflow()
    {
        const long value = (long)int.MaxValue + 1;
        Assert.Throws<OverflowException>(() => Cast<long, int>.Checked(value));
    }
    
    [Fact]
    public void Int64ToInt32_Underflow()
    {
        const long value = (long)int.MinValue - 1;
        Assert.Throws<OverflowException>(() => Cast<long, int>.Checked(value));
    }

    [Fact]
    public void FloatToInt32()
    {
        const float value = MathF.PI;
        var result = Cast<float, int>.Checked(value);
        Assert.Equal(MathF.Floor(value), result);
    }
    
    [Fact]
    public void FloatToInt32_Overflow()
    {
        const float value = float.MaxValue;
        Assert.Throws<OverflowException>(() => Cast<float, int>.Checked(value));
    }
    
    [Fact]
    public void FloatToInt32_Underflow()
    {
        const float value = float.MinValue;
        Assert.Throws<OverflowException>(() => Cast<float, int>.Checked(value));
    }
    
    [Fact]
    public void DoubleToInt64()
    {
        const double value = Math.PI;
        var result = Cast<double, long>.Checked(value);
        Assert.Equal(Math.Floor(value), result);
    }
    
    [Fact]
    public void DoubleToInt64_Overflow()
    {
        const double value = double.MaxValue;
        Assert.Throws<OverflowException>(() => Cast<double, long>.Checked(value));
    }
    
    [Fact]
    public void DoubleToInt64_Underflow()
    {
        const double value = double.MinValue;
        Assert.Throws<OverflowException>(() => Cast<double, long>.Checked(value));
    }
    
}