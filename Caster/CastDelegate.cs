﻿namespace Caster;

public delegate TTo CastDelegate<in TFrom, out TTo>(TFrom from);