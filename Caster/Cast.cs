﻿using System.Linq.Expressions;

namespace Caster;

public static class Cast<TFrom, TTo>
{
    private static CastDelegate<TFrom, TTo>? CheckedCast;
    private static CastDelegate<TFrom, TTo>? UncheckedCast;

    private static CastDelegate<TFrom, TTo> CreateDelegate(bool @checked)
    {
        var p = Expression.Parameter(typeof(TFrom), "p");
        var c = @checked ? Expression.ConvertChecked(p, typeof(TTo)) : Expression.Convert(p, typeof(TTo));
        var l = Expression.Lambda<CastDelegate<TFrom, TTo>>(c, true, p);
        return l.Compile();
    }
    
    
    /// <summary>
    /// Gets or creates a delegate that can be used to cast a value of type <typeparamref name="TFrom"/> to type <typeparamref name="TTo"/>.
    /// </summary>
    /// <param name="checked">If the delegate should check or not overflows</param>
    /// <returns></returns>
    public static CastDelegate<TFrom, TTo> GetDelegate(bool @checked)
    {
        return @checked ? CheckedCast ??= CreateDelegate(true) : UncheckedCast ??= CreateDelegate(false);
    }


    /// <summary>
    /// Cast from <see cref="TFrom"/> to <see cref="TTo"/>"
    /// </summary>
    /// <param name="from">Value to cast</param>
    /// <exception cref="InvalidCastException">Thrown if <see cref="TFrom"/> cannot be casted to <see cref="TTo"/></exception>
    /// <exception cref="OverflowException">Thrown if <see cref="TFrom"/> is too large to be casted to <see cref="TTo"/></exception>
    /// <returns></returns>
    public static TTo Checked(TFrom from) => (CheckedCast ??= CreateDelegate(true))(from);
    
    /// <summary>
    /// Cast from <see cref="TFrom"/> to <see cref="TTo"/>"/> without checking for overflow
    /// </summary>
    /// <param name="from">Value to cast</param>
    /// <exception cref="InvalidCastException">Thrown if <see cref="TFrom"/> cannot be casted to <see cref="TTo"/></exception> 
    public static TTo Unchecked(TFrom from) => (UncheckedCast ??= CreateDelegate(false))(from);
}